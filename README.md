# JL GRID #


### Init Grid table ###

Simple init

```
#!php

new Grid('table_name');
```

Configure columns

```
#!php

new Grid('table_name', array(
    'columns' => array(
        'id',
        'name' => array(
            'sort' => true,
            'filter' => true,
        ),
        'url',
        'city' => array(
            'header' => 'Город',
            'type' => array('model', array('table' => 'city', 'pk' => 'id', 'field' => 'name'))
        ),
        'status' => array(
            'header' => 'Status',
            'type' => array('list', array(1 => 'Активно', 0 => 'Не активно')),
        ),
        'status' => array(
            'header' => 'Date',
            'type' => array('date', 'Y.m.d'),
        ), 
        'price' => array(
            'header' => 'Price',
            'type' => array('price', 2), // 2 sign after ','
        ), 
        'params' => array(
            'header' => 'Params',
            'type' => array('serialize', array('implode' => '|')), // User implode or implode_key to join all items
        ), 
        'JButtons' => array(
            'edit',
            'delete',
            'custom' => array(
                 'url' => '/action/customThing',  // /action/customThing?id=[id]
                 'image' => '/path/to/image.png',
            ),
        )
    ),
)
```
 