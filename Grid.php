<?php

/**
 * Created by PhpStorm.
 * User: User
 * Date: 028 28.01.16
 * Time: 8:21
 * version 1.3 
 *
 * Типы колонок
 * list - Передать список в Grid
 * model - Использовать стороннюю модель
 *
 */

/*
    TEMPLATE

new Grid('table_name', array(
    'class' => 'table table-striped',
    'sort' => 'date',
    'where' => 'status > 0 AND name LIKE :name',
    'params' => array(
        'name' => Grid.'%',
    ),
    'pk' => 'propid',                           // Primary key
    'pagination' => array(
        'count' => 50,
        'page' => 5,                            // Set current page
    ),
    'columns' => array(
        'id',
        'name' => array(
            'sort' => true,
            'filter' => true,
        ),
        'url',
        'city' => array(
            'header' => 'Город',
            'type' => array('model', array('table' => 'city', 'pk' => 'id', 'field' => 'name'))
        ),
        'status' => array(
            'header' => 'Статус',
            'type' => array('list', array(1 => 'Активно', 0 => 'Не активно')),
        ),
        'status' => array(
            'header' => 'Дата',
            'type' => array('date', 'Y.m.d'),
        ),
        'JButtons' => array(
            'edit',
            'delete',
        )
    ),
));

 */
class Grid{

    private $table;
    private $primaryKey = 'id';
    private $tableColumns;
    private $params;
    private $activeColumns = array();
    private $htmlTable;
    private $tableData;
    private $htmlPagination;
    private $tableSort = 'id';
    private $tableWhere = '';
    private $tableWhereParams = array();

    private $imageDelete = '/assets/img/grid_delete.png';
    private $imageEdit = '/assets/img/grid_edit.png';


    private $mbCharset = 'utf-8';
    private $maxDefaultCols = 8;

    /**
     * Языковые данные
     */
    private $lang = array(
        'total_pages' => 'Total pages',
    );

    /**
     * Переменные для пагинации
     */
    private $currentPage = 1;
    private $countOnPage = 20;
    private $totalPages;

    private $modelList = array();


    /**
     *  HTML values
     */
    private $htmlClass = '';
    private $htmlId = 'JLTableGrid_';
    private $htmlStyle = '';

    /**
     * Grid constructor.
     * @param $table
     * @param array $params
     */
    public function __construct($table, array $params = array()){
        $this->params = $params;
        $this->table = $table;
        $this->htmlId .= $table;



        $this->GetColumns();

        $this->ParseParams();

        $this->GenerateTable();

        echo $this->htmlTable;
    }


    /**
     * Генерация TH тегов таблицы
     */
    private function GenerateHeader(){
        $tr = "<thead><tr>";

        foreach($this->activeColumns as $col){
            if(isset($col['header']))
                $name = $col['header'];
            else{
                $name = $col['column'];
                $fLetter = mb_substr($name, 0, 1, $this->mbCharset);
                $name = mb_strtoupper($fLetter, $this->mbCharset).mb_substr($name, 1, 1000, $this->mbCharset);



                if($col['column'] == 'JButtons')
                    $name = 'Actions';
            }

            $link = $_SERVER['REQUEST_URI'];
            $header = '';

            // Сортировка колонок
            if(isset($col['sort'])){
                $link = preg_replace('/(\/?[?&]+page=[0-9]+)/', '', $link);
                $link = preg_replace('/(\/?[?&]+sort=[\w _]+)/', '', $link);
                $link = str_replace('%20DESC', '', $link);

                $sign = '&';
                if(strpos($link, '?') === false)
                    $sign = '?';

                $sortBy = $col['column'];

                if(isset($_GET['sort']) && $_GET['sort'] == $col['column'])
                    $sortBy = $col['column']." DESC";
                $header = '<a href="'.$link.$sign.'sort='.$sortBy.'">'.$name.'</a>';
            }
            else
                $header = $name;

            // Фильтр по колонке
            $filter = '';
            if(isset($col['filter'])){
                if($col['filter'] === true){
                    $filterVal = isset($_GET['Filter'][$col['column']]) ? $_GET['Filter'][$col['column']] : '';
                    $notUse = array('model', 'type', 'action', 'page', 'module');
                    $inpFields = '';
                    foreach($_GET as $key => $t){
                        if(in_array($key, $notUse)) continue;
                        if(empty($t) ) continue;
                        if(is_array($t)){
                            foreach ($t as $k => $it)
                                $inpFields .= '<input type="hidden" name="'.$key.'['.$k.']" value="'.$it.'" />';
                        }
                        else
                        $inpFields .= '<input type="hidden" name="'.$key.'" value="'.$t.'" />';

                    }

                    $filter = '<form class="JLGrid_filter">'.$inpFields.'<input type="text" value="'.$filterVal.'" name="Filter['.$col['column'].']" /></form>';

                }



                $header .= $filter;
            }
            $itemClass = '';
            // если есть класс
            if(isset($col['class']))
                $itemClass = 'class="'.$col['class'].'"';

            $tr .= '<th '.$itemClass.'>'.$header.'</th>';
        }

        $tr .= "</tr></thead>";

        $this->htmlTable .= $tr;
    }





    /**
     * Спарсить параметры, которые передали в Grid
     */
    private function ParseParams(){

        if(isset($this->params['columns'])){
            $this->ParseColumns();
        }
        else{
            $this->DefaultColumns();
        }

        if(isset($this->params['pk']))
            $this->primaryKey = $this->params['pk'];

        if(isset($this->params['sort']))
            $this->tableSort = $this->params['sort'];

        if(isset($_GET['sort']))
            $this->tableSort = $_GET['sort'];

        if(isset($this->params['where']))
            $this->tableWhere = $this->params['where'];

        if(isset($this->params['params']))
            $this->tableWhereParams = $this->params['params'];


        $this->Pagination();


        $this->PrepareData();

        // Берем класс
        if(isset($this->params['class']))
            $this->htmlClass = $this->params['class'];

        // Берем id
        if(isset($this->params['id']))
            $this->htmlId = $this->params['id'];

        // Берем Стили
        if(isset($this->params['style']))
            $this->htmlStyle = $this->params['style'];


    }

    /**
     * Собрать список колонок, которые передаются в параметры
     */
    private function ParseColumns(){

        $colsNameArr = array();
        foreach ($this->tableColumns as $col)
            $colsNameArr[] = $col[0];

        $activeCol = array();
        foreach($this->params['columns'] as $key => $col){

            if(is_int($key) && !is_array($col)){
                $activeCol[] = array('column' => $col);
            }
            else{
                $activeCol[] = array_merge($col, array('column' => $key) );
            }
        }

        $this->activeColumns = $activeCol;

    }


    /**
     * Собрать список используемых колонок, если он не задан
     */
    private function DefaultColumns(){
        $activeCol = array();
        $i = 1;
        foreach ($this->tableColumns as $col){

            if($i == $this->maxDefaultCols) break;

            $activeCol[] = array('column' => $col[0]);

            $i++;
        }

        $this->activeColumns = $activeCol;
    }


    /**
     * Генерация таблицы
     */
    private function GenerateTable(){
        $this->htmlTable = '';

        $this->HeadFilter();


        if($this->tableData){
            $this->htmlTable .= '<table id="'.$this->htmlId.'" class="'.$this->htmlClass.'" style="'.$this->htmlStyle.'">';
            $this->GenerateHeader();

            $this->FillData();
            $this->htmlTable .= '</table>';
            $this->GeneratePagination();
            $this->htmlTable .= $this->htmlPagination;

        }
        else{
            $this->htmlTable .= "<h2>No data</h2>";
        }

//        $this->htmlTable .= $this->htmlPagination;
        $this->Style();


    }


    /**
     * Указание текущих фильтров и отчистка
     */
    private function HeadFilter(){
        $showFilter = false;
        if(isset($_GET['sort']) || isset($_GET['Filter']))
            $showFilter = true;

        if($showFilter)
            $this->htmlTable .= "<div><div class='JLGrid_filter_area'>";

        // Показывать сортировку
        if(isset($_GET['sort'])){
            $this->htmlTable .= "Sorted by ".strtolower($_GET['sort']);
            if(isset($_GET['Filter'])) $this->htmlTable .= '<br />';
        }

        // Показывать фильтры
        if(isset($_GET['Filter'])){
            $this->htmlTable .= "Filter by ";

            $colList = array();
            foreach($_GET['Filter'] as $key => $val)
                $colList[] = $key.' ('.$val.')';

            $this->htmlTable .= implode(', ', $colList);
        }

        if($showFilter){
            $this->htmlTable .= '<a class="JLGrid_clear_button" href="'.$this->ClearLink($_SERVER['REQUEST_URI']).'">Clear</a>';
            $this->htmlTable .= "</div></div>";
        }

    }


    /**
     * Получение данных из БД
     */
    private function PrepareData(){


        if(!empty($this->tableWhere)){
            $this->tableWhere = " WHERE ".$this->tableWhere;
        }

        if(!count($this->tableWhereParams))
            $this->tableWhereParams = false;

        // Подготовка фильтров
        if(isset($_GET['Filter'])){
            if(empty($this->tableWhere))
                $this->tableWhere = " WHERE ";
            else
                $this->tableWhere .= " AND ";

            $paramsArr = $valArr = array();
            foreach($_GET['Filter'] as $key => $val){
                $paramsArr[$key] = '%'.$val.'%';
                $valArr[] = '`'.$key.'` LIKE :'.$key;
            }

            $this->tableWhere .= implode(' AND ', $valArr);
            if(!$this->tableWhereParams)  $this->tableWhereParams = $paramsArr;
            else $this->tableWhereParams = array_merge($this->tableWhereParams, $paramsArr);
        }

        $count = DB::Count("SELECT * FROM {$this->table} {$this->tableWhere}", $this->tableWhereParams);
        $this->totalPages = ceil($count / $this->countOnPage);

        $limit = $this->countOnPage;
        $offset = $this->countOnPage * ($this->currentPage - 1);

        if($offset < 0) $offset = 0;

        $sth = DB::query("SELECT * FROM {$this->table} {$this->tableWhere} ORDER BY {$this->tableSort} LIMIT {$limit} OFFSET {$offset}", $this->tableWhereParams);

        $this->tableData = $sth->fetchAll();

    }


    /**
     * Генерация контента таблицы
     */
    private function FillData(){
        $ret = '<tbody>';
        foreach($this->tableData as $item){
            $ret .= "<tr>";
            foreach($this->activeColumns as $col){

                $itemClass = '';

                if($col['column'] == 'JButtons'){
                    $id = $item[$this->primaryKey];
                    $val = $this->GetItemButtons($id);

                    $ret .= "<td class='JButtons_td'>{$val}</td>";
                    continue;
                }

                $val = $item[$col['column']];
                // Если есть тип поля
                if(isset($col['type']))
                    $val = $this->GetValueByType($col['type'], $val);

                // если есть класс
                if(isset($col['class']))
                    $itemClass = 'class="'.$col['class'].'"';


                $ret .= "<td {$itemClass}>{$val}</td>";
            }
            $ret .= "</tr>";
        }
        $ret .= '</tbody>';

        $this->htmlTable .= $ret;
    }


    /**
     * @param $id
     * @return string
     *
     * Сгенерировать кнопки
     *
     * 
     */
    private function GetItemButtons($id){
        $link = $_SERVER['REQUEST_URI'];

        if(substr($link, -1, 1) != '/')
            $link .= '/';

        $ret = '';

        if(!is_array($this->params['columns']['JButtons'])){
            $ret .= $this->GetEditButton($id, $link);
            $ret .= $this->GetDeleteButton($id, $link);
        }
        elseif(is_array($this->params['columns']['JButtons'])){
            foreach($this->params['columns']['JButtons'] as $key => $but){

                if($key === 'edit' || $but === 'edit')
                    $ret .= $this->GetEditButton($id, $link, $but);
                elseif($key === 'delete' || $but === 'delete')
                    $ret .= $this->GetDeleteButton($id, $link, $but);
                else{
                    $ret .= $this->GetCustomButton($id, $key, $but);
                }
            }
        }


        return $ret;
    }

    private function GetCustomButton($id, $buttonName, $but){

        $link = $but['url'];
        $args = array('link' => $link . '?id=' . $id, 'image' => $but['image']);

        return sprintf('<a href="%s"class="button_%s_grid"><img src="%s" /></a>', $args['link'], $buttonName, $args['image']);

    }
    /**
     * @param $id
     * @param $link
     * @return string
     *
     * Сгенерировать Кнопку удаления
     */
    private function GetDeleteButton($id, $link, $but){

        $delMsg = 'Вы уверены, что хотите удалить зпись #'.$id.'?';
        if(is_array($but)){
            if(isset($but['url'])) $link = $but['url'];
            if(isset($but['message'])) $delMsg = $but['message'];
        }
        else{
            $link = $this->ClearLink($link);
            if(substr($link, -1) == '/')
                $link = substr($link, 0, -1);
            $link .= '/delete/';
            if (substr($link, -1) != '/') $link .= '/';

        }


        $delete = array('link' => $link.(strpos($link, '?') === false ? '?' : '&').'id='.$id, 'image' => $this->imageDelete, 'confirm' => $delMsg);

        if(isset($this->params['columns']['JButtons']['delete']['link']))
            $delete['link'] = $this->params['columns']['JButtons']['delete']['link'];
        if(isset($this->params['columns']['JButtons']['delete']['confirm']))
            $edit['confirm'] = $this->params['columns']['JButtons']['delete']['confirm'];

        return '<a href="'.$delete['link'].'"  onclick="return confirm(\''.$delete['confirm'].'\')"  class="button_edit_grid"><img src="'.$delete['image'].'" /></a>';
    }

    /**
     * @param $id
     * @param $link
     * @return string
     *
     * Сгенерировать кнопку редактирования
     */
    private function GetEditButton($id, $link, $but)
    {


        if(is_array($but)){
            if(isset($but['url'])) $link = $but['url'];
        }
        else{
            $link = $this->ClearLink($link);

            if(substr($link, -1) == '/')
                $link = substr($link, 0, -1);
            $link .= '/edit/';
            if (substr($link, -1) != '/') $link .= '/';
        }

        $edit = array('link' => $link .(strpos($link, '?') === false ? '?' : '&').'id=' . $id, 'image' => $this->imageEdit);

        if(isset($this->params['columns']['JButtons']['edit']['link']))
            $edit['link'] = $this->params['columns']['JButtons']['edit']['link'];

        return '<a href="'.$edit['link'].'"class="button_edit_grid"><img src="'.$edit['image'].'" /></a>';
    }

    /**
     * @param $type
     * @param $val
     * @return string
     *
     * Получить значение по типу может быть списком, может быть другой моделью
     *
     * TODO добавить передачу модели в GRID (опционально)
     */
    private function GetValueByType($type, $val){
        if(is_array($type)){

            $tParams = $type[1];

            if($type[0] == 'list'){
                return $tParams[$val];
            }
            elseif($type[0] == 'date'){
                return date($type[1], $val);
            }
            elseif($type[0] == 'price'){
                if(!$val) $val = 0;
                $val =  number_format($val, $type[1]);
                return $val == 0 ? '' : $val;
            }
            elseif($type[0] == 'json'){
                $val = json_decode($val);

                if(isset($type[1]['implode'])) return implode($type[1]['implode'], $val);
                elseif (isset($type[1]['implode_key'])) {
                    $its = array();
                    foreach($val as $key => $val)
                        if($val == true) $its[] = $key;
                    return implode($type[1]['implode_key'], $its);
                }
            }
            elseif($type[0] == 'serialize'){
                $val = unserialize($val);

                if(isset($type[1]['implode'])) return implode($type[1]['implode'], $val);
                elseif (isset($type[1]['implode_key'])) {
                    $its = array();
                    foreach($val as $key => $val)
                        if($val == true) $its[] = $key;
                    return implode($type[1]['implode_key'], $its);
                }
            }
            elseif($type[0] == 'model'){


                if(!isset($tParams['pk']))
                    $tParams['pk'] = 'id';

                // Получаем нужную модель и сохраняем её, чтобы не вызывать постоянно
                if(!isset($this->modelList[$tParams['table']]))
                    $this->modelList[$tParams['table']] = $this->ListData(DB::query("SELECT * FROM {$tParams['table']}"));

                // Отдаем нужное значение из сохранянной таблицы
                if(isset($this->modelList[$tParams['table']][$val][$tParams['field']]))
                    return $this->modelList[$tParams['table']][$val][$tParams['field']];

                return '';

            }
        }
    }

    /**
     * @param $data
     * @param string $listId
     * @return array
     *
     * Построение списка с идентефикатором в качестве ключа. Взято из библиотеки JL
     */
    private function ListData($data, $listId = 'id'){
        $return = array();
        foreach($data as $item){
            $return[$item[$listId]] = $item;
        }
        return $return;
    }


    /**
     * Подготовка параметров для пагинации
     */
    private function Pagination(){

        // Назначить кол-во на странице
        if(isset($this->params['pagination']['count']))
            $this->countOnPage = $this->params['pagination']['count'];

        // Текущая страница. Либо берем $_GET['page'], либо передаем в массив
        if(isset($this->params['pagination']['page']))
            $this->currentPage = $this->params['pagination']['page'];
        else{
            if(isset($_GET['page']) && $_GET['page'] != 0)
                $this->currentPage = $_GET['page'];
            else
                $this->currentPage = 1;
        }

    }

    /**
     * Генерация пагинации
     */
    private function GeneratePagination(){


        if($this->totalPages <= 1)
            return '';

        $link = $_SERVER['REQUEST_URI'];

        $link = preg_replace('/([?&]+page=[0-9]+)/', '', $link);

        if(substr($link, -1) == '?')  $link = substr($link, 0, -1);
        $sign = '?';
        if(strpos($link, '?') !== false) $sign = '&';

        $link .= $sign.'page=';
        $html = '<div>'.$this->lang['total_pages'].': '.$this->totalPages.'</div>';
        $html .= '<div class="JLGrid_Pagination">';

        if($this->currentPage != 1 ){
            $html .= '<a class="JLGridPagination_first" href="'.$link.'1"><<</a>';
            $html .= '<a class="JLGridPagination_prev" href="'.$link.($this->currentPage - 1).'">&larr;</a>';
        }

        for($i = 1; $i <= $this->totalPages; $i++){

            if($i < ($this->currentPage - 3)) continue;
            if($i > ($this->currentPage + 3)) continue;

            if($i == $this->currentPage)
                $html .= '<span class="JLGridPagination_active">'.$i.'</span>';
            else
                $html .= '<a class="JLGridPagination_page" href="'.$link.$i.'">'.$i.'</a>';

        }

        if($this->currentPage != $this->totalPages ){
            $html .= '<a class="JLGridPagination_next" href="'.$link.($this->currentPage + 1).'">&rarr;</a>';
            $html .= '<a class="JLGridPagination_last" href="'.$link.$this->totalPages.'">>></a>';
        }

        $html .= '</div>';
        $html .= '<form class="JLGridPagination_form"><input type="text" name="page" value="'.$this->currentPage.'" style="width:40px;"><input type="submit" value="GO"></form>';

        $this->htmlPagination = $html;
    }

    /**
     * Собрать список колонок из базы данных
     *
     * TODO переделать на чистый MYSQL или PDO или пох, оставить так
     */
    private function GetColumns(){
        $q = "
            SELECT COLUMN_NAME
            FROM information_schema.COLUMNS
            WHERE TABLE_SCHEMA = DATABASE()
              AND TABLE_NAME = '{$this->table}'
            ORDER BY ORDINAL_POSITION
        ";
        $sth = DB::query($q);
        $this->tableColumns = $sth->fetchAll();
    }

    private function ClearLink($link){


        $link = preg_replace('/(\/?[?&]+sort=[^?&$]+)/', '', $link);
        $link = preg_replace('/(\/?[?&]+sort=)/', '', $link);
        $link = preg_replace('/(\/?[?&]+page=[0-9]+)/', '', $link);
        $link = preg_replace('/(\/?[?&]+page=)/', '', $link);

        $link = preg_replace('/\/?[?&]+Filter[^=]+=[^&?$]+/', '', $link);
        $link = preg_replace('/(\/?[?&]+Filter[^=]+=)/', '', $link);

        $link = str_replace('%20DESC', '', $link);
        return $link;
    }


    private function Style(){

        $this->htmlTable .=
            '<style>
    .JLGrid_Pagination{
        display: inline-block;
    }
    .JLGridPagination_active {
        font-weight: bold;
        padding: 5px;
        font-size: 15px;
    }
    .JLGridPagination_page{
        padding: 5px;
    }
    .JLGridPagination_next, .JLGridPagination_prev,.JLGridPagination_first,.JLGridPagination_last{
        padding: 5px;
        border: 1px solid #ccc;
        margin: 0 2px;
    }
    .JLGridPagination_form {
        display: inline-block;
        margin-left: 20px;
    }
    .JLGridPagination_form input[type="text"] {
        margin: 0;
    }
    .JLGridPagination_form input[type="submit"] {
        height: 30px;
        margin-left: 5px;
    }
    .JLGrid_filter{
        display: inline-block;
        margin: 0;
        padding: 0;
    }
    .JLGrid_filter input{
        max-width: 100px;
        width: 100%;
        margin: 0;
        padding: 0;
        margin-left: 10px;
        padding-left: 5px;
    }
    .JLGrid_clear_button{
        font-size: 16px;
        margin-top: 15px;
        display: block;
    }
    .JLGrid_clear_button:hover{
        color: red;
        border-bottom: 1px dashed red;
        text-decoration: none;
    }
    .JLGrid_filter_area {
        padding: 10px;
        display: inline-block;
        border: 1px solid #ccc;
    }
    .JButtons_td a{margin: 0 3px;}
</style>';

    }



}